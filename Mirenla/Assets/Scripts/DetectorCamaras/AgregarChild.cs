using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgregarChild : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            other.gameObject.transform.SetParent(transform);
        }
    }
}
