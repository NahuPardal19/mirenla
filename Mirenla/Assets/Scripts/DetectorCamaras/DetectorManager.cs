using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DetectorManager : MonoBehaviour
{
    [SerializeField] Detector[] detectors;
    [SerializeField] Color colorActivado;
    [SerializeField] Color colorDesactivado;
    void Start()
    {

    }
    void Update()
    {
        foreach (Detector item in detectors)
        {
            for (int i = item.Zona.transform.childCount - 1; i >= 0; i--)
            {
                if (item.Zona.transform.GetChild(i).gameObject.activeSelf)
                {
                    item.boton.GetComponent<Image>().color = colorActivado;
                    break;
                }
                else
                {
                    item.boton.GetComponent<Image>().color = colorDesactivado; 
                }
            }
        }
    }
}

