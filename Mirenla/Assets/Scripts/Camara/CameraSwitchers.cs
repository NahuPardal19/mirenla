using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using TMPro;

public class CameraSwitchers : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI numeroCamara;
    [SerializeField] List<CinemachineVirtualCamera> Cameras = new List<CinemachineVirtualCamera>();

    public void CambiarACamara(CinemachineVirtualCamera cam)
    {
        foreach (CinemachineVirtualCamera camera in Cameras)
        {
            camera.Priority = 0;
            camera.GetComponent<RotacionCamara>().PuedoRotar = false;
        }
        cam.GetComponent<RotacionCamara>().PuedoRotar = true;
        cam.Priority = 10;

    }

    public void CambiarNumeroDeCamara(int n)
    {
        numeroCamara.text = "CAM " + n; 
    }
}
