using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionCamara : MonoBehaviour
{
    [SerializeField] float sensibilidad;
    [SerializeField] float LimiteRotacionNegativa;
    [SerializeField] float LimiteRotacionPositiva;
    private float rotacionX;
    private float rotacionY;
    public bool PuedoRotar;
    

    private void Awake()
    {
        //Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (PuedoRotar)
        {
            MovimientoHorizontal();
            MovimientoVertical();
        }
    }

    private void MovimientoHorizontal()
    {
        float mouseX = Input.GetAxis("Horizontal") * Time.deltaTime * sensibilidad;
        rotacionX += mouseX;
        rotacionX = Mathf.Clamp(rotacionX, LimiteRotacionNegativa, LimiteRotacionPositiva);
        transform.rotation = Quaternion.Euler(0, rotacionX, 0);
    }

    private void MovimientoVertical()
    {
        float mouseY = Input.GetAxis("Vertical") * Time.deltaTime * sensibilidad;
        rotacionY -= mouseY;
        rotacionY = Mathf.Clamp(rotacionY, -90, 90);
        Camera.main.transform.rotation = Quaternion.Euler(rotacionY, rotacionX, 0);

    }

}
