using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Manager Cegada")]
    [SerializeField] private GameObject PEM;
    private static GameObject PantallaCegar;
    [SerializeField] private GameObject UI;
    private static GameObject PantallaUI;

    void Start()
    {
        PantallaCegar = PEM;
        PantallaUI = UI;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            SoundManager.instancia.ReproducirSonido("Estatica");
        }
    }


    public static IEnumerator CorrutinaCegar(float tiempo,GameObject objeto)
    {

        PantallaCegar.SetActive(true);
        PantallaUI.SetActive(false);
        yield return new WaitForSeconds(tiempo);
        PantallaCegar.SetActive(false);
        PantallaUI.SetActive(true);
        objeto.SetActive(false);
        //SoundManager.instancia.PausarSonido("Estatica");
    }
}
