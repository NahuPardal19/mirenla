using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Life : MonoBehaviour
{
    public static float life;
    private static float maxlife = 20;
    [SerializeField] private Slider slider;
    public static Life instance;
    //public static bool desvinculado;
    void Awake()
    {
        life = maxlife;
        gameObject.SetActive(true);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable()
    {

    }
    void Start()
    {
        life = maxlife;
        CambiarvidaMaxima(maxlife);
    }

    void Update()
    {
        if (!Mirenla.EnMira) life = life - Time.deltaTime;
        if (Mirenla.EnMira && life < maxlife) life = life + Time.deltaTime;
        CambiarVidaActual(life);
        if (life <= 0)
        {
            GameOver.Loss = true;
        }
        Destruir();
    }

    public void CambiarvidaMaxima(float vidaMaxima)
    {
        slider.maxValue = vidaMaxima;
    }

    public void CambiarVidaActual(float vidaActual)
    {
        slider.value = vidaActual;
        print(vidaActual);
    }

    public static void ReiniciarVida()
    {
        life = maxlife;
    }

    public static void AumentarVidaMax(int moreLife)
    {
        maxlife += moreLife;
    }

    public void Destruir()
    {
        if (SceneManager.GetActiveScene().name == "GAME OVER" || SceneManager.GetActiveScene().name == "MENU")
        {
            print("misma escena");
            gameObject.GetComponentInChildren<Canvas>().enabled = false;
          GameOver.Loss=false;
        }
        else gameObject.GetComponentInChildren<Canvas>().enabled = true;
    }
}
