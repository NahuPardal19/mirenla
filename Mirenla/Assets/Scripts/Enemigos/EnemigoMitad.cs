using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoMitad : MonoBehaviour
{
    [Header("COSTADOS")]
    [SerializeField] GameObject[] costados;
    private bool PuedeMorir;

    [Header("EXPLOSION")]
    public float TiempoPem = 0;
    [SerializeField] float tiempoCegada;
    [SerializeField] float[] TiemposDeExplosion;
    [SerializeField] Color[] FasesColores;
    public bool PemTirada;
    private Renderer rend;
    private float disolucion = -1.2f;
    private bool EnMira;
    void Start()
    {
        rend = GetComponent<Renderer>();
        PuedeMorir = false;
    }

    void Update()
    {
        Destruirse();
        PEM();
        foreach (GameObject obj in costados)
        {
            if (obj.activeSelf)
            {
                PuedeMorir = false;
                break;
            }
            else PuedeMorir = true;
        }

    }
    private void PEM()
    {
        TiempoPem += Time.deltaTime / 2;
        if (TiempoPem >= TiemposDeExplosion[0] && TiempoPem < TiemposDeExplosion[1]) rend.material.SetColor("_Color_Base", FasesColores[0]);
        else if (TiempoPem >= TiemposDeExplosion[1] && TiempoPem < TiemposDeExplosion[2]) rend.material.SetColor("_Color_Base", FasesColores[1]);
        else if (TiempoPem >= TiemposDeExplosion[2] && TiempoPem < TiemposDeExplosion[3]) rend.material.SetColor("_Color_Base", FasesColores[2]);
        else if (TiempoPem >= TiemposDeExplosion[3] && PemTirada)
        {
            //SoundManager.instancia.ReproducirSonido("Estatica");
            StartCoroutine(GameManager.CorrutinaCegar(tiempoCegada, gameObject));
            PemTirada = false;
        }
    }
    private void OnEnable()
    {
        TiempoPem = 0;
        PemTirada = true;
        gameObject.GetComponent<Renderer>().material.SetColor("_Color_Base", FasesColores[3]);
        disolucion = -1.2f;
        gameObject.GetComponent<Renderer>().material.SetFloat("_Disolution", disolucion);
        foreach (GameObject g in costados)
        {
            print("hOLA");
            g.SetActive(true);
        }
        PuedeMorir = false;
    }

    private void Destruirse()
    {
        if (disolucion < 0.7 && PuedeMorir && EnMira)
        {
            disolucion += Time.deltaTime / 2f;
            rend.material.SetFloat("_Disolution", disolucion);
        }
        else if (disolucion >= 0.7) gameObject.SetActive(false); ;
    }

    private void OnBecameVisible()
    {
        EnMira = true;
    }
    private void OnBecameInvisible()
    {
        EnMira = false;
    }
}
