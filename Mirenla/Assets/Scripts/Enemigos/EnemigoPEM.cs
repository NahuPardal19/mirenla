using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoPEM : MonoBehaviour
{
    public bool EsMirado;
    private float disolucion = -1.2f;
    private Renderer rend;
    private float TiempoPem = 0;
    private bool PemTirada;

    [SerializeField] float tiempoCegada;
    [SerializeField] float [] TiemposDeExplosion;
    [SerializeField] Color[] FasesColores;

    private void Start()
    {
        PemTirada = true;
        rend = GetComponent<Renderer>();
    }
    void Update()
    {
        if (EsMirado) Destruirse();
        if (!EsMirado) PEM();
    }


    private void OnBecameInvisible()
    {
        EsMirado = false;
    }

    private void OnBecameVisible()
    {
        EsMirado = true;
    }

    private void Destruirse()
    {
        if (disolucion < 0.7)
        {
            disolucion += Time.deltaTime / 2;
            rend.material.SetFloat("_Disolution", disolucion);
        }
        else if (disolucion >= 0.7) gameObject.SetActive(false); ;
    }

    private void PEM()
    {
        TiempoPem += Time.deltaTime / 2;
        if (TiempoPem >= TiemposDeExplosion[0] && TiempoPem < TiemposDeExplosion[1]) rend.material.SetColor("_Color_Base", FasesColores[0]);
        else if (TiempoPem >= TiemposDeExplosion[1] && TiempoPem < TiemposDeExplosion[2]) rend.material.SetColor("_Color_Base", FasesColores[1]);
        else if (TiempoPem >= TiemposDeExplosion[2] && TiempoPem < TiemposDeExplosion[3]) rend.material.SetColor("_Color_Base", FasesColores[2]);
        else if (TiempoPem >= TiemposDeExplosion[3] && PemTirada)
        {
            //SoundManager.instancia.ReproducirSonido("Estatica");
            StartCoroutine(GameManager.CorrutinaCegar(tiempoCegada,gameObject));
            PemTirada = false;
        } 
    }

    private void OnEnable()
    {
        TiempoPem = 0;
        PemTirada = true;
        gameObject.GetComponent<Renderer>().material.SetColor("_Color_Base", FasesColores[3]);
        disolucion =-1.2f; 
        gameObject.GetComponent<Renderer>().material.SetFloat("_Disolution", disolucion);
    }
}
