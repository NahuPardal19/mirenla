using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoMedusa : MonoBehaviour
{
    private float tiempoVista=0;
    private bool EnMira;
    private bool EmpezoAverlo;

    [SerializeField] private float tiempoVerlo;
    [SerializeField] private float tiempoCegar;

    void Start()
    {
       
    }

    void Update()
    {
        if (EnMira)
        {
            tiempoVista += Time.deltaTime;
        }
        if(!EnMira && EmpezoAverlo)
        {
            gameObject.SetActive(false);
        }
        if(tiempoVista >= tiempoVerlo)
        {
            //SoundManager.instancia.ReproducirSonido("Estatica");
            StartCoroutine(GameManager.CorrutinaCegar(tiempoCegar,gameObject));
        }
    }

    private void OnBecameVisible()
    {
        EnMira = true;
        EmpezoAverlo = true;
    }
    private void OnBecameInvisible()
    {
        EnMira = false;
    }
    private void OnEnable()
    {
        tiempoVista = 0;
        EmpezoAverlo = false;
    }
}
