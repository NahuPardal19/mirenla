using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EnemigoMinijuego : MonoBehaviour
{
    private float tiempo;
    private bool enMira;

    void Start()
    {
        
    }

    void Update()
    {
        if (enMira)
        {
            tiempo += Time.deltaTime;
        }
        if (tiempo >= 1f)
        {
            SceneManager.LoadScene(2);
        }
    }

    private void OnBecameVisible()
    {
        enMira = true;
    }
}
