using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desactivar : MonoBehaviour
{
    private bool EmpezoAverlo;
    private bool EnMira;

    private void Update()
    {
        if (EmpezoAverlo && !EnMira)
        {
            gameObject.SetActive(false);
        }
    }
    private void OnBecameVisible()
    {
        EnMira = true;
        EmpezoAverlo = true;
    }
    private void OnBecameInvisible()
    {
        EnMira = false;
    }
    private void OnEnable()
    {
        EnMira = false;
        EmpezoAverlo = false;
    }
}
