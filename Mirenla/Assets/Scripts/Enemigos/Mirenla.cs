using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mirenla : MonoBehaviour
{
    public static bool EnMira;

    private void OnBecameVisible()
    {
        EnMira = true;
    }

    private void OnBecameInvisible()
    {
        EnMira = false;
    }

}
