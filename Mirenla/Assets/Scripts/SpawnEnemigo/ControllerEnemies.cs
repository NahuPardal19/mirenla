using UnityEngine;

public class ControllerEnemies : MonoBehaviour
{
    [Header("ENEMIGOS")]

    GameObject EnemigoMirenla;
    [SerializeField] Transform[] SpawnsEnemigoMirenla;
    [SerializeField] GameObject[] enemigosPem;
    [SerializeField] GameObject[] enemigosMedusa;
    [SerializeField] GameObject[] enemigosMinijuego;
    [SerializeField] GameObject[] enemigosMedio;

    [Header("TIEMPO")]
    [SerializeField] float respawningTimer;
    [SerializeField] float numRandomMin;
    [SerializeField] float numRandomMax;
    int cantidadEnemigosPem;
    int cantidadEnemigosMedusa;
    int cantidadEnemigosMedio;

    void Start()
    {
        EnemigoMirenla = GameObject.Find("Enemigo Mirenla");
        TransportarEnemigoMirenla();
    }

    void Update()
    {
        SpawnearEnemigo();
    }

    public void SpawnearEnemigo()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            int numeroRandom = Random.Range(1, 5);

            if (numeroRandom == 1)
            {
                ActivarEnemigoPEM();
            }
            else if (numeroRandom == 2)
            {
                ActivarMedusa();
            }
            else if (numeroRandom == 3)
            {
                ActivarEnemigoMinijuego();
            }
            else if (numeroRandom == 4)
            {
                ActivarMedio();
            }
            TransportarEnemigoMirenla();
            respawningTimer = Random.Range(numRandomMin, numRandomMax);
        }

    }

    public void TransportarEnemigoMirenla()
    {
        int n = Random.Range(0, SpawnsEnemigoMirenla.Length);
        EnemigoMirenla.transform.position = SpawnsEnemigoMirenla[n].transform.position;
    }
    public void ActivarEnemigoPEM()
    {
        cantidadEnemigosPem = 0;
        foreach (GameObject enemies in enemigosPem)
        {
            if (enemies.activeSelf) cantidadEnemigosPem += 1;
        }
        if (cantidadEnemigosPem < 5)
        {
            int n = Random.Range(0, enemigosPem.Length);

            if (!enemigosPem[n].activeSelf)
            {
                enemigosPem[n].SetActive(true);
                ActivarMedusa();
            }
            else ActivarEnemigoPEM();
        }
    }
    public void ActivarMedusa()
    {
        cantidadEnemigosMedusa = 0;
        foreach (GameObject enemies in enemigosMedusa)
        {
            if (enemies.activeSelf) cantidadEnemigosMedusa += 1;
        }
        if (cantidadEnemigosMedusa < 4)
        {
            int n = Random.Range(0, enemigosMedusa.Length);

            if (!enemigosMedusa[n].activeSelf)
                enemigosMedusa[n].SetActive(true);
            else ActivarMedusa();
        }
    }
    public void ActivarEnemigoMinijuego()
    {
        foreach (GameObject enemies in enemigosMinijuego)
        {
            enemies.SetActive(false);
        }
        int n = Random.Range(0, enemigosMinijuego.Length);

        if (!enemigosMinijuego[n].activeSelf)
        {
            enemigosMinijuego[n].SetActive(true);
            ActivarMedusa();
        }
        else ActivarEnemigoMinijuego();
    }
    public void ActivarMedio()
    {
        cantidadEnemigosMedio = 0;
        foreach (GameObject enemies in enemigosMedio)
        {
            if (enemies.activeSelf) cantidadEnemigosMedio += 1;
        }
        if (cantidadEnemigosMedio < 2)
        {
            int n = Random.Range(0, enemigosMedio.Length);

            if (!enemigosMedio[n].activeSelf)
            {
                enemigosMedio[n].SetActive(true);
                ActivarMedusa();
            }
            else ActivarMedio();
        }
    }

}
