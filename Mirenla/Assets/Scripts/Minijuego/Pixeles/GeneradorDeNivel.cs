using UnityEngine;
public class GeneradorDeNivel : MonoBehaviour
{
    public Texture2D[] mapa;
    public ColorAPrefab[] colorMappings;
    private int n;
    void Start()
    {
        n = Random.Range(0, mapa.Length);
        GenerarNivel();
    }
    private void GenerarNivel()
    {
        
        for (int x = 0; x < mapa[n].width; x++)
        {
            for (int y = 0; y < mapa[n].height; y++)
            {
                GenerarTile(x, y);
            }
        }
    }
    void GenerarTile(int x, int y)
    {
        Color pixelColor = mapa[n].GetPixel(x, y);
        if (pixelColor.a == 0)
        {
            return;
        }
        foreach (ColorAPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector2 position = new Vector2(x, y);
                Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
            }
        }
    }
}
