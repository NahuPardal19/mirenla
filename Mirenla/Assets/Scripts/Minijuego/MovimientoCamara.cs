using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCamara : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private float direccionHorizontal = 0f;
    [SerializeField] private float direccionVertical = 0f;
    [SerializeField] private float speed = 5f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        direccionHorizontal = Input.GetAxisRaw("Horizontal");
        direccionVertical = Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector3(direccionHorizontal * speed, direccionVertical * speed,0);


    }
}
